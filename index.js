const puppeteer = require('puppeteer');

(async () => {
  const browserWSEndpoint = 'ws://127.0.0.1:8080';
  const browser = await puppeteer.connect({ignoreHTTPSErrors: true, browserWSEndpoint: browserWSEndpoint});
  console.log(browser._connection._ws);
  const page = await browser.newPage();
  await page.goto('https://example.com');
  //await page.screenshot({path: 'example.png'});

  await browser.close();
})();

//{"ws":"ws://0.0.0.0:40421/devtools/browser/aa3e34d2-ebe1-4707-b606-3546ce53864e","ip":"34.207.202.72"}