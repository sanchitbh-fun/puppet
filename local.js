const puppeteer = require('puppeteer');
const debugMqtt = require('debug')('lambda:remote');
const mqtt = require('./mqtt.js');
const Browser = require('Puppeteer/lib/Browser');

const mqttClient = mqtt.mqttClient();
let _lastId = 0;
let browser;
let connection = {
    send: (method, params = {}) => {
        const msg = JSON.stringify({id: ++_lastId, method, params});
        console.log("send: ", msg);
        mqttClient.publish(mqtt.MQTT_TOPIC, msg);
    },
    createSession: (target) => {
        console.log("target: ", target);
    }
};

mqttClient.on('message', (topic, message) => {
    let json = JSON.parse(message);
    debugMqtt('mqtt message: ' + json);
    browser._connection.send(json.method, json.params);
});

mqttClient.on('connect', () => {
    mqttClient.subscribe(mqtt.MQTT_TOPIC);
    debugMqtt('connected to iot mqtt websocket');

    try {
        browser = new Browser(connection, true);
    } catch (err) {
        console.log("err1: ", err.toString());
    }

    try {
        browser.newPage().then((page) => {
            console.log(page);
        });
    } catch (err) {
        console.log("err2: ", err.toString());
    }
});

/*browser._connection._ws.on('message', (msg) => {
    console.log("msg: ", msg);
});*/

//const page = await browser.newPage();
/*await page.goto('https://example.com');
await page.screenshot({path: 'example.png'});

console.log("ws: ", browser.wsEndpoint());

await browser.close();*/
