const puppeteer = require('puppeteer');
const debugMqtt = require('debug')('lambda:remote');
const mqtt = require('./mqtt.js');
const TIMEOUT = 1000 * 60 * 20;

let browserWs;
const browser = puppeteer.launch()
    .then((browser) => {
        const mqttClient = mqtt.mqttClient();
        browserWs = browser._connection._ws;
        console.log("browserWs: ", browserWs.url);

        mqttClient.on('connect', () => {
            mqttClient.subscribe(mqtt.MQTT_TOPIC);
            debugMqtt('connected to iot mqtt websocket');
        });

        mqttClient.on('message', (topic, message) => {
            let json = JSON.parse(message);
            debugMqtt('mqtt message: ' + json);
            browser._connection.send(json.method, json.params);
        });

        browser._connection._ws.on('message', (message) => {
            debugMqtt('browser message: ' + message.toString());
            mqttClient.publish(mqtt.MQTT_TOPIC, message.toString());
        });

        debugMqtt('waiting for messages');
    }).catch((err) => {
        console.log("error: ", err.toString());
    });

setTimeout(() => 1, TIMEOUT);

/*const page = await browser.newPage();
await page.goto('https://example.com');
await page.screenshot({path: 'example.png'});

console.log("ws: ", browser.wsEndpoint());

await browser.close();*/
//(async () => {})();